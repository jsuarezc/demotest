'use strict';

angular.module('mobileminderApp', [
  "ngSanitize",
	"angularLocalStorage",
  "auth",
  "pascalprecht.translate",
  "ngMap",
  "stripe",
	"directives",


  "commonServices",
  "settingServices",
  "loginServices", 
  "deviceServices",
  "recentActivityServices",
  "locationServices",
  "callAndTextServices",
  "galleryServices",
  "appServices",
  "internetServices",
  
  "loginCtrls",
  "tourCtrls",
  "recentActivityCtrls",
	"locatinCtrls",
	"callAndTextCtrls",
	"galleryCtrls",
	"appCtrls",
	"internetCtrls",
  "paymentCtrls"

])
.config(['$stateProvider', '$urlRouterProvider','$translateProvider','$httpProvider',
function($stateProvider, $urlRouterProvider, $translateProvider, $httpProvider) {

  //Enable cross domain calls
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];


  //$urlRouterProvider.otherwise('/location');
  $urlRouterProvider.when('', '/activity');
  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'views/login.html',
      controller: 'loginCtrl',
      authenticate: false,      
    })
    .state('403', {
      url: '/403',
      templateUrl: 'views/403.html',
      authenticate: false,      
    })
    .state('activity', {
      url: '/activity',
      templateUrl: 'views/activity.html',
      controller: 'recentActivityCtrl',
      authenticate: true
    })
    .state('location', {
      url: '/location',
      templateUrl: 'views/location.html',
      controller: 'locatinCtrl',
      authenticate: true
    })
    .state('callAndText', {
      url: '/callAndText',
      templateUrl: 'views/callAndText.html',
      controller: 'callAndTextCtrl',
      authenticate: true
    })
    .state('gallery', {
      url: '/gallery',
      templateUrl: 'views/gallery.html',
      controller: 'galleryCtrl',
      authenticate: true
    })
    .state('apps', {
      url: '/apps',
      templateUrl: 'views/apps.html',
      controller: 'appCtrl',
      authenticate: true
    })
    .state('internet', {
      url: '/internet',
      templateUrl: 'views/internet.html',
      controller: 'internetCtrl',
      authenticate: true
    })
    .state('nodevices', {
      url: '/nodevices',
      templateUrl: 'views/nodevices.html',
      //controller: 'internetCtrl',
      authenticate: true
    })
    .state('expired', {
      url: '/expired',
      templateUrl: 'views/expired.html',
      controller: 'paymentCtrl',
      authenticate: true,      
    })
    .state('buynow', {
      url: '/buynow',
      templateUrl: 'views/buynow.html',
      controller: 'paymentCtrl',
      authenticate: true,      
    });

    $translateProvider
    .translations('de', translationsDe);


    // redirect to login page if the status of server response is 401 or 403
    $httpProvider.interceptors.push(function($q, $location,$window,storage) {
        return {
            'responseError': function(response) {
                if(response.status === 401 ) {
                    //console.log("forbidden page.");
                    storage.clearAll();
                    localStorage.clear();
                    delete window.intercomSettings;

                    //console.log(storage.get('oauthToken'));
                    //console.log(storage.get('default_device'));
                    $window.location.href = 'login.html';
                    //return $q.reject(response);
                }
                else if(response.status === 403){
                    console.log("Its 403 webpage");
                    $location.path('/403')
                }
                return $q.reject(response);
            }
        };
    });


    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/json';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  
   
}]).run(function ($rootScope,$injector,$window,storage,$state,$stateParams,$location,AuthService) {
    
  
    console.log("Inside the run method");
    $injector.get("$http").defaults.transformRequest = function(data, headersGetter) {
        if (storage.get('oauthToken')) headersGetter()['Authorization'] = "JWT "+storage.get('oauthToken'); 
      
        if (data) {
            return angular.toJson(data);
        }
    };

    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      
     
      if(!AuthService.isAuthenticated()){//user is not authenticated
          
          //$rootScope.showDirectives = false;// if the user is not authenticated dont show him any direcitives
          
          if(toState.authenticate){//route needed authentication
             //$state.transitionTo("login");            
             //event.preventDefault();
             storage.clearAll();
             localStorage.clear();
             delete window.intercomSettings;
             $window.location.href = 'login.html'; 
          }
          
      }
      else{   
          if(toState.authenticate){
              //stop the default events if the default device is null

              var default_device = storage.get('default_device');

              if(!default_device){//if the acount has no devices
                 if(toState.url != "/nodevices"){
                     //console.log('inside the event preventDefault');
                     $state.transitionTo('nodevices', {});
                     event.preventDefault(); 
                 }                
                    
              }
              else{
                  if(!default_device.active){// if device has expired
                      if(toState.url != '/expired'){
                        //console.log("Your device expired :)");
                        //toState.url = "/expired"; 
                        $state.transitionTo('expired', {});
                        event.preventDefault();  
                      }

                  }  
                  else{
                     //console.log("I am redirecting here");
                     if(toState.url == '/expired' || toState.url == '/nodevices'  ){
                        $state.transitionTo('activity', {});
                        event.preventDefault();  
                     }
                  }              
              } //end if(default_device)
          } //end if(authenticate)       

      }  
    });
   
  });