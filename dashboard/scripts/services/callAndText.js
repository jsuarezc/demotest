'use strict';

var callAndTextServices = angular.module('callAndTextServices',['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */
callAndTextServices.factory('ContactsFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/contacts', {}, {
        query: {method: 'GET',params:{id:'@id',order_by:'contacted_at',order_type:'desc'}, isArray:true},
        //create: {method: 'POST'}
    })
}]);

callAndTextServices.factory('ContactsInfo',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/contacts/:id', {}, {
        query: {method: 'GET'},
        //create: {method: 'POST'}
    })
}]);


callAndTextServices.factory('TextsFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/contacts/:id/texts', {}, {
        query: {method: 'GET', isArray:true},
        //create: {method: 'POST'}
    })
}]);

callAndTextServices.factory('CallsFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/contacts/:id/calls', {}, {
        query: {method: 'GET', isArray:true},
        //create: {method: 'POST'}
    })
}]);

callAndTextServices.factory('DefaultFlaggedWordsFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/flagged_words/default', {}, {
        query: {method: 'GET',isArray:true},
    })
}]);

callAndTextServices.factory('FlaggedWordsFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/custom_flagged_words', {}, {
        query: {method: 'GET',isArray:true},
    })
}]);

callAndTextServices.factory('FlaggedWordsFactoryCreate',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/flagged_words', {}, {
        create: {method: 'POST'},
    })
}]);

callAndTextServices.factory('FlaggedWordsFactoryDelete',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/flagged_words/:id', {}, {
        delete: {method: 'DELETE',isArray:true},
    })
}]);

callAndTextServices.factory('DefaultFlaggedWordsFactoryDelete',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/exclude_flagged_words', {}, {
        delete: {method: 'DELETE',isArray:true},
    })
}]);





