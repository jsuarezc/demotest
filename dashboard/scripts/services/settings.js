'use strict';

var settingServices = angular.module('settingServices', ['ngResource']);

/** 
 * Password - Update
 * 
 */
settingServices.factory('PasswordFactory',['$resource','Constants',function ($resource,Constants) {
     return $resource( Constants.get('baseUrl')+'/api/accounts/current_account/password', {}, {
        update: {method: 'PUT'},
    })
}]);


/** 
 * Account Details - Get,Update
 * 
 */
settingServices.factory('AccountDetailsFactory',['$resource','Constants',function ($resource,Constants) {
     return $resource( Constants.get('baseUrl')+'/api/accounts/current_account', {}, {
        query: {method: 'GET'},
        update: {method: 'PUT'},
    })
}]);