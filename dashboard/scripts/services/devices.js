'use strict';

var deviceServices = angular.module('deviceServices', ['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */
deviceServices.factory('DevicesFactory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices?order_by=create_at', {}, {
        query: {method: 'GET', isArray:true},
    })
}]);

deviceServices.factory('DeviceFactory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id', {}, {
        update: {method: 'PUT',params: {id:'@id'}},
    })
}]);

deviceServices.factory('DeviceCommandFactory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/commands', {}, {
        create: {method: 'POST',params: {id:'@id'}},
    })
}]);

