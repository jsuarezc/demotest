'use strict';

var commonServices = angular.module('commonServices', ['ngResource']);

/**
 * Global Constants
 *
 */
commonServices.service('Constants',function() {
    // define UI constants here
    var constants = {
        'baseUrl': 'https://api-v1.mobileminder.com',
        //'baseUrl': 'http://localhost:5000',

    };
    return {
        /*set : function(level) {
            constants['currentLevel'] = level;
        },*/
        get : function(key) {
            return constants[key];
        }
    };
});

/**
 * Levels - Shared between controllers
 *
 */
commonServices.factory('commonFactory', function ($resource) {
    return {
        secsToTime: function (secs) {

            var sec_num = parseInt(secs, 10); // don't forget the second parm
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}

            var time;

            if(hours == "00" && minutes == "00"){
                time    = seconds+" "+"secs";
            }
            else if(hours == "00" && minutes != "00"){
                time    = minutes +" : "+ seconds + " "+"mins";
            }
            else{
                time    = hours +" : "+ minutes + " "+"hours";
            }
            return time;

        },
        unixToDate: function(timestamp){
            var a = new Date(timestamp*1000);
            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = date + ',' + month + ' ' + year + ' ' + hour + ':' + min  ;

            return time;
        },

        b64toBlob : function(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                   byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }
            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        }

    };
});


commonServices.factory('Amazon',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/aws/token', {}, {
        query: {method: 'GET', isArray:false},
        //create: {method: 'POST'}
    })
}]);

commonServices.factory('StripeCredentils',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/stripe/publish_key', {}, {
        query: {method: 'GET', isArray:false},
        //create: {method: 'POST'}
    })
}]);


commonServices.factory('StripeCurrentUser',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/stripe/current', {}, {
        query: {method: 'GET', isArray:false},
        //create: {method: 'POST'}
    })
}]);

commonServices.factory('StripePayment',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/stripe/payment', {}, {
       create: {method: 'POST'}
    })
}]);

commonServices.factory('StripeCoupon',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/stripe/coupon?coupon_code=:coupon_code', {}, {
      query: {method: 'GET', params:  {coupon_code:'@coupon_code'}, isArray:false}
    })
}]);


commonServices.factory('IntercomAccount',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/intercom/account/current', {}, {
       query: {method: 'GET', isArray:false},
    })
}]);

commonServices.factory('IntercomDevice',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/intercom/device/:id', {}, {
       query: {method: 'GET', isArray:false},
    })
}]);

