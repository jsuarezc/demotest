'use strict';

var galleryServices = angular.module('galleryServices', ['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */
galleryServices.factory('GalleryFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/commands', {}, {
        create: {method: 'POST'}
    })
}]);

