var app = angular.module('auth',[]);

app.service('AuthService', ['storage',function(storage){

    //#TODO: Need to base this userIsAuthenticated based on cookie value or authentication
    var userIsAuthenticated = false;

    //#TODO: Need to set the value to true based on a cookie which internally based on server response
    this.setUserAuthenticated = function(value) {
        userIsAuthenticated = value;
    };

    this.isAuthenticated = function() {

    	return (storage.get('oauthToken'))?true:false;
    };

}]);