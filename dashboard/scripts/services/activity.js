'use strict';

var recentActivityServices = angular.module('recentActivityServices', ['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */

recentActivityServices.factory('RecentContacts',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/contacts', {}, {
        query:{method:'GET', params:  {id:'@id',per_page:6,order_by:'contacted_at',order_type:'desc'}, isArray:true},
    })
}]);

recentActivityServices.factory('RecentWebHistory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/web_history', {}, {
        query: {method: 'GET',params:{page:1,per_page:6,order_by:'start_at',order_type:'desc'}, isArray:true},
    })
}]);

recentActivityServices.factory('RecentAppFactory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/apps', {}, {
        query: {method: 'GET',params:{page:1,per_page:6,order_by:'start_at',order_type:'desc'}, isArray:true},
    })
}]);

recentActivityServices.factory('RecentLocationAlerts',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/geofences/logs', {}, {
        query:{method:'GET', params:  {id:'@id',per_page:6,order_by:'timestamp',order_type:'desc'}, isArray:true},
    })
}]);
