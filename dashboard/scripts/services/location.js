'use strict';

var locationServices = angular.module('locationServices', ['ngResource']);

/** 
 * Current Location - Get
 * 
 */
locationServices.factory('CurrentLocation',['$resource','Constants',function ($resource,Constants) {
     return $resource( Constants.get('baseUrl')+'/api/devices/:id/locations', {}, {
        query: {method: 'GET', params: {id:'id',page:1,per_page:1,order_by:'timestamp',order_type:'desc'}, isArray:true},
    })
}]);


/** 
 * Alert Zones - Get
 * 
 */
locationServices.factory('AlertZones',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/geofences', {}, {
        query: {method: 'GET',params:{id:'id'}, isArray:true},
    })
}]);

/** 
 * Alert Zone - Create
 * 
 */
locationServices.factory('NewAlertZone',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/geofences', {}, {
        create: {method: 'POST'},
    })
}]);

/** 
 * Alert Zone - Delete
 * 
 */
locationServices.factory('AlertZone',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/geofences/:id', {}, {
        update:{method:'PUT', params: {id: '@id'}},
        delete: {method: 'DELETE'},
    })
}]);


/** 
 * Alert Zone - Delete
 * 
 */
locationServices.factory('GeofenceLog',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/geofences/logs', {}, {
        query:{method:'GET', params: {id: '@id'}, isArray:true},
    })
}]);

