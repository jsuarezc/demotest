'use strict';

var loginServices = angular.module('loginServices', ['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */
loginServices.factory('LoginFactory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/accounts/login', {}, {
        query: {method: 'GET', isArray:true},
        create: {method: 'POST'}
    })
}]);

loginServices.factory('SignUpFactory',['$resource','Constants', function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/accounts/signup', {}, {
        query: {method: 'GET', isArray:true},
        create: {method: 'POST'}
    })
}]);