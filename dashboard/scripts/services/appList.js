'use strict';

var appListServices = angular.module('appServices', ['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */
appListServices.factory('AppFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/apps', {}, {
        query: {method: 'GET', isArray:true},
        //create: {method: 'POST'}
    })
}]);

appListServices.factory('BlockAppFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/apps/:id', {}, {
        update: {method: 'PUT',params: {id:'@id'}},
    })
}]);

appListServices.factory('UnblockAppFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/apps/unblock', {}, {
        update: {method: 'PUT'},
    })
}]);

