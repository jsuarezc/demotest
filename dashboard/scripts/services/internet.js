'use strict';

var internetServices = angular.module('internetServices', ['ngResource']);

/** 
 * Levels - Shared between controllers
 * 
 */
internetServices.factory('WebFactory',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/web_history', {}, {
        query: {method: 'GET', isArray:true},
        create: {method: 'POST'}
    })
}]);

internetServices.factory('Whitelist',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/devices/:id/allowed_domains', {}, {
        query: {method: 'GET', isArray:true},
        create: {method: 'POST'}
    })
}]);

internetServices.factory('WhitelistCreate',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/webs/allowed_domains', {}, {
        create: {method: 'POST'}
    })
}]);

internetServices.factory('WhitelistDelete',['$resource','Constants',function ($resource,Constants) {
    return $resource( Constants.get('baseUrl')+'/api/webs/allowed_domains/:id', {}, {
        delete: {method: 'DELETE'}
    })
}]);
