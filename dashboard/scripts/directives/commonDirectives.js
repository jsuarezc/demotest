'use strict';

var dirController = angular.module('directives', ['ui.router','ngImgCrop']);
dirController.directive('myHeader',function() {
  return {
    restrict: 'E',
    templateUrl: '../../views/header.html',
    controller:  function($scope,$state,$window,$stateParams,storage,$translate,$timeout,Amazon,IntercomAccount,IntercomDevice,commonFactory,DevicesFactory,DeviceFactory,AccountDetailsFactory,PasswordFactory) {

        $scope.unlock_code = storage.get('unlock_code');
        $scope.defaultProfilePic = 'https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/images/avatars/default';

        /**
         * Intercom Details - Get
         *
         */
        $scope.intercomDetails = function(){

            //console.log("testing the intercom");
             /**
               * Intercom Account Details - GET
               *
               */
               IntercomAccount.query(
                  function(data){
                      //console.log(success);
                      //console.log("Intercom Account details success");
                      //console.log(data);

                      $.each(data, function(key, value){
                          //console.log(key);
                          //console.log(value);
                          //if(key != '$promise' && key != '$resolve'){
                             window.intercomSettings[key] = value;

                         // }

                      });

                      Intercom('update',window.intercomSettings);

                      //console.log(window.intercomSettings);

                  },
                  function(error){
                      console.log("Intercom Account Details failed");
                      console.log(error);
                  }
               );

               if(storage.get('default_device')){
                    /**
                     * Intercom Device Details - Get
                     *
                     */
                     IntercomDevice.query({id:storage.get('default_device').id},
                        function(data){
                            //console.log(data);
                            //console.log("Intercom Device details success");

                            if(!$.isEmptyObject(data))
                            {
                                window.intercomSettings["company"] = data;
                            }

                             Intercom('update',window.intercomSettings);

                        },
                        function(error){
                            console.log("Intercom Device Details failed");
                            console.log(error);
                        }
                     );
               }


        }





         if(!storage.get('access_key')){
              //console.log("Aws creadentials dont exists");
              Amazon.query(
                  function(data){
                      //console.log("Amazon credentials - Successful");
                      //console.log(data);
                      if(data){
                          storage.set('access_key',data.access_key);
                          storage.set('secret_key',data.secret_key);
                          storage.set('session_token',data.session_token);
                      }
                      else{
                          console.log("Something went wrong with amazon credentials");
                      }

                  },
                  function(err){
                      console.log("Amazon creadentials - failed");
                      console.log(err);
                  }
              );

          }

          /* get device list after the user logged in*/
          DevicesFactory.query(function(data){
              if(data.length > 0){
                for (var k=0;k<data.length;k++){
                  if(data[k].id){
                    data[k].index = k;
                   }
                 }
                 $scope.deviceList = data;
                 if(!storage.get('default_device')){
                    //console.log("Default device is not set");
                    storage.set('default_device',data[0]);
                    $scope.currentDevice = data[0];
                    //console.log(data[0]);
                 }

                 //console.log($scope.currentDevice.id);
             }
           });

          /**
           * Set Current Device on Users Selection
           * @param {Object} device
           */
           $scope.setCurrentDevice = function (device){

               $scope.currentDevice = device;
               storage.set('default_device',device);
               //console.log("its gonna redirect now !!!!");
               //console.log($state.current.url);

               //
               //$location.path($state.current.url);
               //console.log(device.active);

               if(device.active){
                  //console.log($state.current.url);
                  if($state.current.url == '/expired'){
                      $state.transitionTo('activity', $stateParams, {
                        reload: true,
                        inherit: false,
                        notify: true
                      });
                  }
                  else{
                      $state.transitionTo($state.current, $stateParams, {
                        reload: true,
                        inherit: false,
                        notify: true
                      });
                  }

               }
               else{
                  //console.log("device is expired boioioioioioooi");
                  $state.go('expired');
               }

               $scope.intercomDetails();



            }


            //#TODO : Need to include the settings functionality.
            $scope.signOut = function(){

                // clear all localStorage values
                storage.clearAll();
                localStorage.clear();
                delete window.intercomSettings;

                //console.log(storage.get('oauthToken'));
                //console.log(storage.get('default_device'));
                $window.location.href = 'login.html';
                //$state.transitionTo("login");
            }

            $scope.changeLanguage = function(langKey){
                 $translate.use(langKey);
            };


          /**
           * Account Details - Get
           *
           */
           $scope.accountDetails = AccountDetailsFactory.query(
               function(data){
                //nsole.log(data);

                $scope.accountName = (data.name)?data.name:null;
                //$scope.emergency_numbers = (data.emergency_numbers.length > 0)?data.emergency_numbers:[,,,];
                $scope.emergency_numbers = (data.emergency_numbers != null)?data.emergency_numbers:[,,,];
                //console.log(data.emergency_numbers);

              },
              function(error){
                console.log("Account Details Error : "+error);
              }
           );







          /**
           * Account Details - Update
           *
           */
           $scope.updateAccountDetails = function(){
               var accountInfo = {};

               $scope.accountInfoSpinner = true;
               $scope.emergency_numbers = $.grep($scope.emergency_numbers,function(n){ return(n) });


                 //validation the account name not to be empty and checking if it has updated
                  if($scope.accountDetails.name){
                    if($scope.accountDetails.name != $scope.accountName){
                      accountInfo.name = $scope.accountDetails.name;
                    }
                  }
                  else{
                        //console.log("Nothing to update");
                        $scope.accountInfoSpinner = false;
                        $scope.accountUpdateInfo = "Account Name cannot be empty!";
                        $timeout(function(){
                          $scope.accountUpdateInfo = '';
                        }, 3000);
                        return;
                  }

                 //validation the emergency numbers not to be empty and checking if they have updated
                 if($scope.emergency_numbers.length !== 0){
                    if($scope.emergency_numbers != $scope.accountDetails.emergency_numbers){
                        accountInfo.emergency_numbers = $scope.emergency_numbers;
                   }
                 }
                 else{
                  $scope.accountInfoSpinner = false;
                  $scope.accountUpdateInfo = "Emergency Numbers cant be emtpy!";
                  $timeout(function(){
                    $scope.accountUpdateInfo = '';
                  }, 3000);
                  return;
                }


                if(Object.getOwnPropertyNames(accountInfo).length !== 0){
                      // console.log("Object is not empty");

                      AccountDetailsFactory.update(accountInfo,
                        function(data){
                             // console.log(data);
                             $scope.accountDetails = data;
                             $scope.accountName = (data.name)?data.name:null;
                             $scope.emergency_numbers = (data.emergency_numbers.length > 0)?data.emergency_numbers:[,,,];
                             $scope.accountInfoSpinner = false;
                             $scope.accountUpdateSuccess = "Account Info Updated Successfully";
                             $timeout(function(){
                               $scope.accountUpdateSuccess = '';
                             }, 3000);
                             //console.log("Account Details Update- Success")
                             //$scope.originalDetails = data;
                           },
                           function(error){
                              //console.log(error);
                              //console.log("Account Details Update - Failure");
                              $scope.accountUpdateError = "Something went wrong";
                              $timeout(function(){
                               $scope.accountUpdateError = '';
                             }, 3000);
                            }
                            );
                 }

            }

          /**
           * Password - Update
           *
           */
           $scope.updatePassword = function(){
              $scope.passwordSpinner = true;
              if($scope.newPassword != $scope.currentPassword ){
                PasswordFactory.update( {
                 "password" : $scope.currentPassword,
                 "new_password" : $scope.newPassword,
                 "new_password_confirm" : $scope.confirmPassword
               },
               function(data){
                $scope.passwordSpinner = false;
                //console.log(data);
                //console.log("Update Password - Success");
                $scope.passwordSuccess = "Password updated Successfully.";
                $timeout(function(){
                  $scope.passwordSuccess = '';
                }, 3000);
              },
              function(error){
                $scope.passwordSpinner = false;
                //console.log(error);
                //console.log("Update Password - Failure");
                $scope.passwordError = "Something went wrong";
                $timeout(function(){
                 $scope.passwordError = '';
               }, 3000);
              }
              );
              }
              else{
                //current password and new password cant be the same
                $scope.passwordError = "New Password cant be same as Current Password!";
                $timeout(function(){
                  $scope.passwordError = '';
                }, 3000);
              }

          }





          $scope.deviceImage='';
          $scope.deviceCroppedImage = '';
          $scope.deviceDiv = [];
          $scope.uploadFile;
          //$scope.areaType = 'square';


          /**
           * Device Profile Picture - Crop
           *
           */
           $scope.cropImage = function(img){

              var parse = $.parseHTML(img);
              var deviceInfo = JSON.parse(img.getAttribute("device"));

              $scope.$apply(function(){
                $scope.deviceDiv[deviceInfo.index] = true;
              });

              //console.log("Inside the upload image");
              var file = img.files[0];

              var reader = new FileReader();
              reader.onload = function (evt) {
                $scope.$apply(function($scope){
                  $scope.deviceImage = evt.target.result;
                });
              };
              reader.readAsDataURL(file);
              $scope.uploadFile = file;

          }

          /**
           * Device Profile Picture Crop - Cancel
           *
           */
          $scope.cancelCrop = function(index){
            document.getElementById('uploadImage_'+index).value= null;
            $scope.deviceImage='';
            $scope.deviceCroppedImage = '';
            $scope.deviceDiv[index]
            $scope.deviceDiv[index] = false;
            $scope.uploadFile = null;
          }


          /**
           * Device Info - Update
           *
           */
           $scope.updateDeviceInfo = function(device,deviceCroppedImage){
                //console.log("Going to update the device");
                //console.log(device);
                //console.log(device.profile_icon_url);
                //console.log(deviceCroppedImage);

                $scope.deviceSpinner = true;

                //if the profile picture is chaged,
                if($scope.uploadFile){
                  $scope.uploadImageS3(device,deviceCroppedImage);
                }
                //updates our server
                $scope.deviceFactoryUpdate(device);

          }

          /**
           * Profile Picture S3 - Update
           *
           */
          $scope.uploadImageS3 = function(device,deviceCroppedImage){


              var timestamp = new Date().getTime();

              var b64Data = deviceCroppedImage.substring(deviceCroppedImage.indexOf(',') + 1);
              var imageBlob = commonFactory.b64toBlob(b64Data, $scope.uploadFile.type);

              //console.log(storage.get('access_key'));


              AWS.config.update({
                  accessKeyId: storage.get('access_key'),
                  secretAccessKey: storage.get('secret_key'),
                  sessionToken:storage.get('session_token'),
                  region: 'eu-west-1',
                  logger: 'console'
              });

              var params = {
                Bucket: 'media-mobileminder-com',
                Key: 'images/avatars/'+$scope.currentDevice.id,
                Body: imageBlob,
                ContentType: $scope.uploadFile.type,
                CacheControl: 'max-age=86400, must-revalidate',
                StorageClass: 'REDUCED_REDUNDANCY',
              };
              // create the AWS.Request object
              var bucket = new AWS.S3();
              bucket.putObject(params, function(err, data) {
                if(err){
                     //console.log(err, err.stack); // an error occurred
                     //console.log("Image Upload failed");
                     $scope.deviceSpinner = false;
                     device.failure = true;
                       $timeout(function(){
                       $scope.failure = false;
                     }, 3000);
                   }
                   else{
                     //console.log(data);           // successful response
                     //console.log("Image Upload Successful");
                     $scope.$apply(function () {
                       device.avatar_url = 'https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/images/avatars/'+$scope.currentDevice.id+'?'+timestamp;
                       device.profile_icon =  'https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/images/avatars/'+$scope.currentDevice.id;
                     });

                     $scope.deviceFactoryUpdate(device);
                   } //endif
                 });


            }


          /**
           * Profile Picture Heroku - Update
           *
           */
          $scope.deviceFactoryUpdate = function(device){
              var params = {id:device.id,name:device.name,avatar_url:device.profile_icon,upload_image_by_3g:device.upload_image_by_3g,receive_notification:device.receive_notification}

                DeviceFactory.update(params,
                    function(data){
                     //console.log("Device Update successful");
                     //console.log(data);

                     $scope.deviceSpinner = false;
                     device.success = true;
                     $timeout(function(){
                        device.success = false;
                      }, 3000);
                     $scope.cancelCrop(device.index);

                   },
                   function(error){
                       //console.log("Device Update Failure");
                       //console.log(error);
                       $scope.deviceSpinner = false;
                       device.failure = true;
                       $timeout(function(){
                        $scope.failure = false;
                      }, 3000);
                   }
                );
          }

          $scope.intercomDetails();




        },
      };
    });
dirController.directive('myNavBar',function() {
  return {
    restrict: 'E',
    templateUrl: '../../views/nav-bar.html',
    controller:  function($scope,storage) {
        $scope.currentDevice = storage.get('default_device');
	       //#TODO : Need to get the highlight tab functionality
        $scope.openModal = function() {
          $('#accountSettingModal').modal('show');
        };

      },
    };
  });

dirController.directive('myFooter', function() {
  return {
    restrict: 'E',
    templateUrl: '../../views/footer.html',
    controller:  function($scope,$location) {

        /**
         * Nav Bar - Highlight Current Tab
         * @param {String} - tabName
         * @return {Boolean}
         */
         $scope.currentTab = function(tabName){
           return "/"+tabName == $location.path() ? true : false;
         };
       },
     };
   });

dirController.directive('ngMatch', function() {
  return {
    restrict: 'A',
    templateUrl: '../../views/footer.html',
    controller:  function($scope,$location) {


    },
  };
});

dirController.directive('pwCheck', [function () {
  return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
      var firstPassword = '#' + attrs.pwCheck;
      elem.add(firstPassword).on('keyup', function () {
        scope.$apply(function () {
          var v = elem.val()===$(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });
    }
  }
}]);


