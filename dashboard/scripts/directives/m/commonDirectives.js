'use strict';

var dirController = angular.module('directives', ['ui.router','ngImgCrop']);
dirController.directive('myHeader',function() {
  return {
    restrict: 'E',
    templateUrl: 'views/header.html',
    controller:  function($scope,$state,$window,$stateParams,storage,$translate,IntercomAccount,IntercomDevice,$timeout,commonFactory,DevicesFactory,DeviceFactory,AccountDetailsFactory,PasswordFactory) {


                 /**
         * Intercom Details - Get
         *
         */
        $scope.intercomDetails = function(){

            //console.log("testing the intercom");
             /**
               * Intercom Account Details - GET
               *
               */
               IntercomAccount.query(
                  function(data){
                      //console.log(success);
                      //console.log("Intercom Account details success");
                      //console.log(data);

                      $.each(data, function(key, value){
                          //console.log(key);
                          //console.log(value);
                          //if(key != '$promise' && key != '$resolve'){
                             window.intercomSettings[key] = value;
                         // }

                      });

                      Intercom('update',window.intercomSettings);

                  },
                  function(error){
                      console.log("Intercom Account Details failed");
                      console.log(error);
                  }
               );

               if(storage.get('default_device')){
                    /**
                     * Intercom Device Details - Get
                     *
                     */
                     IntercomDevice.query({id:storage.get('default_device').id},
                        function(data){
                            //console.log(data);
                            //console.log("Intercom Device details success");

                            if(!$.isEmptyObject(data))
                            {
                                window.intercomSettings["company"] = data;
                            }

                            Intercom('update',window.intercomSettings);

                        },
                        function(error){
                            console.log("Intercom Device Details failed");
                            console.log(error);
                        }
                     );
               }


        }


          $scope.defaultProfilePic = 'https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/images/avatars/default';



          /* get device list after the user logged in*/
          DevicesFactory.query(function(data){
              if(data.length > 0){
                for (var k=0;k<data.length;k++) {
                  if(data[k].id){
                    data[k].index = k;
                   }
                 }
                 $scope.deviceList = data;
                 if(!storage.get('default_device')){
                    //console.log("Default device is not set");
                    storage.set('default_device',data[0]);
                    $scope.currentDevice = data[0];
                    //console.log(data[0]);
                 }
             }
           });

          /**
           * Set Current Device on Users Selection
           * @param {Object} device
           */
           $scope.setCurrentDevice = function (device){
               $scope.currentDevice = device;
               storage.set('default_device',device);
               //console.log("its gonna redirect now !!!!");
               //console.log($state.current.url);

               //
               //$location.path($state.current.url);
               //console.log(device.active);

               if(device.active){
                  //console.log($state.current.url);
                  if($state.current.url == '/expired'){
                        $state.transitionTo('activity', $stateParams, {
                          reload: true,
                          inherit: false,
                          notify: true
                        });
                  }
                  else{
                      $state.transitionTo($state.current, $stateParams, {
                        reload: true,
                        inherit: false,
                        notify: true
                      });
                  }

               }
               else{
                  //console.log("device is expired boioioioioioooi");
                  $state.go('expired');
               }

                $scope.intercomDetails();



               //console.log($window);
               //$window.location.reload();
               //console.log(storage.get('default_device'));
            }


            //#TODO : Need to include the settings functionality.
            $scope.signOut = function(){

                storage.clearAll();
                localStorage.clear();

                //console.log(storage.get('oauthToken'));
                //console.log(storage.get('default_device'));
                $window.location.href = '../login.html';
                //$state.transitionTo("login");
            }


            var destination = $('#bodywrapper').offset();
            $scope.openNavigationBar = function(){

                //$('.bodyOverlay').css({height: $('#bodywrapper').height()});
                $('.bodyOverlay').css({top: destination.top});
                $('#navSlider').toggle();
                if ($('#navSlider').is(':visible')) {
                   $('.bodyOverlay').show();
                }else{
                   $('.bodyOverlay').hide();
                }

            };

            /**
             * Prevent the scroll event on showing nav bar
             */
            $('html, body').on('touchmove', function(e){
                 //prevent native touch activity like scrolling
                if ($('#navSlider').is(':visible') && $('.bodyOverlay').is(':visible')) {
                  e.preventDefault();
                }

            });

            $scope.intercomDetails();


        },
      };
    });
dirController.directive('myNavBar',function() {
  return {
    restrict: 'E',
    templateUrl: 'views/nav-bar.html',
    controller:  function($scope,storage) {

        $scope.currentDevice = storage.get('default_device');

	      //#TODO : Need to get the highlight tab functionality
        $scope.openModal = function() {
          $('#accountSettingModal').modal('show');
        };


        $scope.closeNavBar = function(){
          //console.log("Closing the nav bar");
          $('#navSlider').hide();
          $('.bodyOverlay').hide();
        }

      },
    };
  });

dirController.directive('myFooter', function() {
  return {
    restrict: 'E',
    templateUrl: 'views/footer.html',
    controller:  function($scope,$location) {

        /**
         * Nav Bar - Highlight Current Tab
         * @param {String} - tabName
         * @return {Boolean}
         */
         $scope.currentTab = function(tabName){
           return "/"+tabName == $location.path() ? true : false;
         };
       },
     };
   });

dirController.directive('ngMatch', function() {
  return {
    restrict: 'A',
    templateUrl: 'views/footer.html',
    controller:  function($scope,$location) {


    },
  };
});

dirController.directive('pwCheck', [function () {
  return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
      var firstPassword = '#' + attrs.pwCheck;
      elem.add(firstPassword).on('keyup', function () {
        scope.$apply(function () {
          var v = elem.val()===$(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });
    }
  }
}]);


