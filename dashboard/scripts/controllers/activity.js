'use strict';

var app = angular.module('recentActivityCtrls', ['ui.router']);

app.controller('recentActivityCtrl',['$scope','$timeout','storage','commonFactory','Amazon','IntercomAccount','IntercomDevice','RecentContacts','RecentLocationAlerts','RecentWebHistory','RecentAppFactory','BlockAppFactory', function ($scope,$timeout,storage,commonFactory,Amazon,IntercomAccount,IntercomDevice,RecentContacts,RecentLocationAlerts,RecentWebHistory,RecentAppFactory,BlockAppFactory) {





    $scope.recentWebsites = [];
    $scope.recentContacts = [];
    $scope.recentAlerts = [];
    $scope.recentPhotos = [];
    $scope.imageListUrls = [];
    $scope.recentApps = [];

    $scope.currentDevice = storage.get('default_device');
    $scope.defaultProfilePic = 'https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/images/contacts/default';



    /**
     * Amazon Credentials - Get
     *
     */


    Amazon.query(
        function(data){
            //console.log("Amazon credentials - Successful");
            //console.log(data);
            if(data){
                storage.set('access_key',data.access_key);
                storage.set('secret_key',data.secret_key);
                storage.set('session_token',data.session_token);
                $scope.$broadcast('AmazonCredentials', data);

            }
            else{
                console.log("Something went wrong with amazon credentials");
            }

        },
        function(err){
            //console.log("Amazon creadentials - failed");
            //console.log(err);
        }
    );


    $scope.$on('AmazonCredentials', function(event, data) {


        AWS.config.update({
            accessKeyId: data.access_key,
            secretAccessKey: data.secret_key,
            sessionToken:data.session_token,
            region: 'eu-west-1',
            logger: 'console'
        });

        // create the AWS.Request object
        var bucket = new AWS.S3();
        var param = {Bucket: 'media-mobileminder-com',Prefix:'images/devices/'+$scope.currentDevice.id+'/',Delimiter:'/'};

        bucket.listObjects(param,function (err, data) {
            if(err) {
                console.log("Get Images Failed");
                console.log(err);
            }else {
                //console.log("Get Images Successful");
                //console.log(data.Contents);
                //console.log(data.Contents.length);
                if(data.Contents.length > 0){
                    angular.forEach(data.Contents, function(value, key) {
                     //console.log(value.Key);
                        if(key < 6){
                             //var params = {Bucket: 'media.mobileminder.com', Key: value.Key};
                             //var url = bucket.getSignedUrl('getObject', params);
                             var url = "https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/"+value.Key
                             $scope.imageListUrls.push({ id:key,url:url,key:value.Key});
                        }
                     });
                    $scope.$apply(function () {
                        $scope.recentPhotos = $scope.imageListUrls;
                        //console.log($scope.recentPhotos);
                    });
                }
                else{
                    $scope.noRecentPhotos = true;
                }

            }
        });

    });


    /**
     * Recent Contacts - Get
     *
     */
    RecentContacts.query({id:$scope.currentDevice.id},
        function(data){
            //console.log("Recent Contacts Successful");
            //console.log(data);
            //console.log(data.length);
            if(data.length > 0){
                $scope.recentContacts = data;
            }
            else{
                $scope.noRecentContacts = true;
            }

        },
        function(error){
            //console.log("Recent Contacts Failed");
            //console.log(error);
        }
    );



	/**
	 * Recent Web History - Get
	 *
	 */
    RecentWebHistory.query({id:$scope.currentDevice.id},
    	function(data){
    		//console.log("Recent Web History Successful");
    		//console.log(data.length);
            if(data.length > 0){
                angular.forEach(data,function(value,index){
                    //console.log(value.end_at);
                    if(value.end_at){
                         //value.browsed_at = commonFactory.unixToDate(value.browsed_at);
                         value.duration = commonFactory.secsToTime(value.duration);
                         value.start_at = commonFactory.unixToDate(value.start_at);
                         //console.log(value);
                         $scope.recentWebsites.push(value);
                         //console.log($scope.recentWebsites);
                    }

                });
                //console.log($scope.recentWebsites);
            }
            else{
                $scope.noRecentWebsites = true;
            }


    	},
    	function(error){
    		//console.log("Recent Web History Failed");
    		//console.log(error);
    	}
    );

    /**
	 * Recent App History - Get
	 *
	 */
    RecentAppFactory.query({id:$scope.currentDevice.id},
    	function(data){
    		//console.log("Recent Apps History Successful");
    		//console.log(data.length);
            if(data.length){
                angular.forEach(data,function(value,index){
                    //console.log(value);
                    if(value.duration){
                         value.duration = commonFactory.secsToTime(value.duration);
                         //console.log(value.browsed_at);
                         $scope.recentApps.push(value)
                    }

                });

                //console.log($scope.recentApps);
            }
            else{
                //console.log("Inside the no recent apps");
                $scope.noRecentApps = true;
            }


    	},
    	function(error){
    		console.log(error);
    	}
    );

    /**
	 * Recent App History - Update
	 *
	 */
    $scope.updateApp = function(app,index){

        var blockStatus;

        blockStatus = app.block_status == "blocked"?'unblocked':'blocked';
        BlockAppFactory.update({id:app.id,'block_status':blockStatus},function(data){
                //console.log("App Updated successfully");
                data.duration = commonFactory.secsToTime(data.duration);
                $scope.recentApps[index] = data;
            },function(error){
                console.log(error);
        });

    }

    /**
	 * Recent Device Locations - Get
	 *
	 */
    RecentLocationAlerts.query({id:$scope.currentDevice.id},
    	function(data){
    		//console.log("Device Location Logs - Success");
    		//console.log(data.length);
            if(data.length){
                for (var k=0; k<data.length; k++){
                    if(data[k].timestamp){
                        data[k].timestamp = commonFactory.unixToDate(data[k].timestamp);
                        $scope.recentAlerts.push(data[k]);
                    }
                }

            }
            else{
                //console.log("Inside the no alerts");
                $scope.noRecentAlerts = true;
            }

    	},
    	function(error){
    		//console.log("Device Location Logs - Failed");
    		//console.log(error);

    	}
    );



  }]);
