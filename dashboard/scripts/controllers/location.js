'use strict';

var app = angular.module('locatinCtrls', ['ui.router','google-maps']);

app.controller('locatinCtrl',['$scope','storage','AlertZones','$timeout','NewAlertZone','AlertZone','CurrentLocation','GeofenceLog','commonFactory', function ($scope,storage,AlertZones,$timeout,NewAlertZone,AlertZone,CurrentLocation,GeofenceLog,commonFactory) {

	$scope.currentDevice = storage.get('default_device');
	$scope.alertZones = [];//array to save the alertZones
	$scope.markersList = [];//array to save corresponding markers to the alert zones
	$scope.geofenceLog = [];//array to save the geofence logs
	$scope.disableZoneEdit = {};
	$scope.sliderValue = 100;

	$scope.polygonOverlays = [];

	$scope.mapBounds = new google.maps.LatLngBounds();

	var marker = new google.maps.Marker({
			  		    draggable: true,
				   		//animation: google.maps.Animation.BOUNCE,
				   		icon: '../images/location/pin_map2.png'
			      }),

		circle = new google.maps.Circle({
		     			radius:  $scope.sliderValue,    // 10 miles in metres
						strokeColor: '#0195ff',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#0195ff',
						fillOpacity: 0.35,
				  }),
		infowindow = new google.maps.InfoWindow();




	/**
	 * Current Location - Get
	 * @param {String} currentDevice.id
	 */

	CurrentLocation.query({'id':$scope.currentDevice.id},
		function(data){

			if(data.length > 0){

				$scope.currentLocation = new google.maps.LatLng(data[0].latitude,data[0].longitude);
				var currentLocationMarker = new google.maps.Marker({
			        position: $scope.currentLocation,
	       			map: $scope.map,
	       			icon: '../images/location/pin_map.png'
			        //draggable: true,
					//animation: google.maps.Animation.BOUNCE,
			   	}),

			    myInfowindow = new google.maps.InfoWindow({
			        content: $scope.currentDevice.name+' is here on '+commonFactory.unixToDate(data[0].timestamp),
			        maxWidth:'auto'
				});
				google.maps.event.addListener(currentLocationMarker, 'click', function() {
				    myInfowindow.open($scope.map,currentLocationMarker);
				});

				$scope.currentMarkerLocation = currentLocationMarker;
				$scope.currentMarkerLocationInfo = myInfowindow;

				$scope.map.setZoom(13);

				$scope.deviceCurrentLocation();


			}
			else{
				//console.log("there is no current locatoin")
				$scope.mapCenter = new google.maps.LatLng(0,0);
			}

		},
		function(error){
			console.log('Get Map Center - Failed');
			console.log(error);
		}
	);

	/**
	 * Alerts - Get
	 * @param {String} currentDevice.id
	 */

	GeofenceLog.query({id:$scope.currentDevice.id},function(data){
		//console.log(data);
		//console.log("Geofence Log GET - Successful");

		if(data.length > 0){
        for (var k=0; k<data.length; k++){
    				if(data[k].timestamp){
    					data[k].timestamp = commonFactory.unixToDate(data[k].timestamp);
    					$scope.geofenceLog.push(data[k]);
    				}
  		  }
		    //console.log($scope.geofenceLog);
		}
		else{
	        $scope.alertsInfo = "No Alert Zones.";
		}

	},
	function(error){
		console.log(error);
		console.log("Geofence Log GET - Failed")
	});


	/**
	 * Alert Zones - Get
	 * @param {String} currentDevice.id
	 */
	AlertZones.query({'id':$scope.currentDevice.id},
		function(data){
			//console.log("Alert Zones Successful");
			//console.log(data);

			if(data.length > 0){
				var bounds = new google.maps.LatLngBounds();

        for (var k=0; k<data.length; k++) {
					if(data[k].id){
						data[k].markerIndex = k;
						var obj = $scope.mapIt(data[k]);
						//console.log(obj.getPath().getAt(0))
						bounds.extend(obj.getPath().getAt(0));
					}

		    }

        $scope.map.fitBounds(bounds);

				$scope.alertZones = data;
			}


			$scope.init();

			$scope.deviceCurrentLocation();


		},
		function(error){
			console.log(error);
			console.log("Get alertzones failure");
		}
	);




	/**
	 * Show the sidebar menu - OnClick
	 *
	 */
    $scope.openSideBar = function(){
		$('#alerts').slideToggle();
		//console.log("side bar toggle");

	};

	/**
	 * Show the device current location - OnClick
	 *
	 */
	$scope.deviceCurrentLocation = function(){
		//console.log("device current location");
		//$scope.currentMarkerLocation.set('animation',google.maps.Animation.BOUNCE);
		//$scope.map.setZoom(8);
		//console.log($scope.currentLocation);
		if($scope.currentLocation){
			$scope.map.panTo($scope.currentLocation);
	    	$scope.currentMarkerLocationInfo.open($scope.map,$scope.currentMarkerLocation);
		}


	};

	/**
	 * New Alert Zone - Toggle
	 * @param {String} alertZoneName
	 * @param {String} sliderValue - Radius of the alert zone.
	 */
	$scope.toggleNewZone = function(){
		//console.log("gonna place a marker on the map.");
		$('.alertZone').removeClass('in');//close all the other alertzones
		//if($scope.currentIndex != null){
		//	$scope.toggleMarker('close',$scope.currentIndex);
		//}
		$scope.clearMap();
		$scope.newAlertZone = ($scope.newAlertZone)?false:true;
		if($scope.newAlertZone){
			$scope.map.drawingManager.setMap($scope.map);
		}
		else{
			$scope.map.drawingManager.setMap(null);

		}
		//$scope.toggleNewMarker();
	};

	/**
	 * New Marker - Toggle
	 *
	 */

	 /*
	$scope.toggleNewMarker = function(){
		if($scope.newAlertZone){
			var latlang = $scope.map.getCenter();
			//console.log(latlang.lat());
			//console.log(latlang.lng());
			marker.set('position',new google.maps.LatLng(latlang.lat(),latlang.lng()));
			marker.set('animation',google.maps.Animation.BOUNCE);
	       	marker.set('map',$scope.map);
	       	circle.set('map',$scope.map);
            circle.bindTo('center',marker,'position');
		}else{
			marker.setMap(null);
			circle.setMap(null);
		}
	}*/

	/**
	 * Existing Marker - Toggle
	 * @param {String} action
	 * @param {String} index.
	 */

	/*
	$scope.toggleMarker = function(action,index){
		if(action == "open"){
			//$scope.map.setZoom(15);
			$scope.map.panTo($scope.markersList[index].position);
			$scope.markersList[index].set('animation',google.maps.Animation.BOUNCE);
			$scope.markersList[index].set('draggable', true);
			$scope.currentIndex = index;
		}
		else if(action == "close"){
			$scope.markersList[index].range.set('radius',$scope.originalRadius);
			$scope.markersList[index].set('position', $scope.originalPosition);
			$scope.markersList[index].set('animation',false);
			$scope.markersList[index].set('draggable', false);
		}
	}
	*/


	/**
	 * Edit Alert Zone - Toggle
	 * @param {String} alertZoneName
	 * @param {String} sliderValue - Radius of the alert zone.
	 */
	$scope.openZone = function(zone,index){

		if($scope.currentZoneId){
			$scope.disableZoneEdit[$scope.currentZoneId] = false;
			$('.'+$scope.currentZoneId).removeClass('in');
		}
		$scope.disableZoneEdit[zone.id] = true;
		$scope.currentZoneId = zone.id;

		//close the new alert zone if its opened
		$scope.newAlertZone = false;
		//$scope.toggleNewMarker();

		//close all the other alertzones if opened
		$('.alertZone').removeClass('in');
		//if($scope.currentIndex != null){
		//	$scope.toggleMarker('close',$scope.currentIndex);
		//}


		//open the current alertzone
		$('.'+zone.id).addClass('in');

        //$scope.originalRadius = $scope.markersList[zone.markerIndex].range.radius;
		$scope.originalPosition = $scope.markersList[zone.markerIndex].position;

		$scope.map.panTo($scope.markersList[zone.markerIndex].getPath().getAt(0));
		//$scope.toggleMarker('open',zone.markerIndex);
	}
	$scope.closeZone = function(zone,index){
		$scope.disableZoneEdit[zone.id] = false;
		$('.'+zone.id).removeClass('in');
		//$scope.toggleMarker('close',zone.markerIndex);

	}



	/**
	 * Alert Zone - Update
	 * @param {Object} zone
	 *
	 */
	$scope.updateAlertZone = function(zone,index,currentRadiusValue){
		$scope.geofenceSpinner = true;
		//zone.latitude = $scope.markersList[index].position.k;
		//zone.longitude = $scope.markersList[index].position.B;

		//zone.radius = ($scope.updateZoneRadius)?$scope.updateZoneRadius:zone.radius;

		AlertZone.update(zone,
			function(data){
				//console.log("Alert Zone Update - Successful");
				//console.log(data);
				$scope.geofenceSpinner = false;
				$scope.zoneResponseSuccess = "Alert Zone Updated Successfully.";
				$timeout(function(){
		           $scope.zoneResponseSuccess = null;
		        },3000);
				$scope.alertZones.splice(index, 1);
				//$scope.markersList.splice(index, 1);
				//delete the marker from the map
				//$scope.markersList[index].range.setMap(null);
				$scope.markersList[index].setMap(null);

				$scope.alertZones.push(data);
				data.markerIndex = index;
				$scope.mapIt(data);

				if($scope.currentZoneId){
					$scope.disableZoneEdit[$scope.currentZoneId] = false;
					$('.'+$scope.currentZoneId).removeClass('in');
				}


			},
			function(error){
				//console.log("Alert Zone Update Error");
				//console.log(error);
				$scope.zoneResponseError = "Something went wrong.";
				$timeout(function(){
		           $scope.zoneResponseError = null;
		        },3000);
			}
		);
	}

	/**
	 * Alert Zone - Delete
	 * @param {id} Zone id
	 *
	 */
	$scope.deleteZone = function(zone,index){

		$scope.geofenceSpinner = true;
		AlertZone.delete({'id':zone.id},
			function(data){
				//console.log("AlertZone Delete - Successful");
				//console.log(data);

				$scope.geofenceSpinner = false;

				//console.log($scope.alertZones);
				//console.log($scope.markersList);
				//console.log(index);
				//console.log(zone);

				$scope.alertZones.splice(index, 1);

				//delete the marker from the map
				//$scope.markersList[zone.markerIndex].range.setMap(null);
				$scope.markersList[zone.markerIndex].setMap(null);

				$scope.zoneResponseSuccess = "Alert Zone Deleted Successfully.";
				$timeout(function(){
		           $scope.zoneResponseSuccess = null;
		        },3000);
			},
			function(error){
				//console.log("AlertZone Delete - Failure");
				//console.log(error);
				$scope.zoneResponseError = "Something went wrong.";
				$timeout(function(){
		           $scope.zoneResponseError = null;
		        },3000);
			}
		)

	};

	/**
	 * Alert Zone - Create
	 * @param {String} alertZoneName
	 * @param {String} sliderValue - Radius of the alert zone.
	 */
	$scope.createAlertZone = function(alertZoneName){

		//console.log($scope.updateZoneRadius);
		//console.log(marker);
		//console.log(marker.position.lat());
		//console.log(marker.position.lng());

		if($scope.polygonOverlays.length > 0){

			$scope.geofenceSpinner = true;
			var newZone = {
				'device_id':$scope.currentDevice.id,
				'name':alertZoneName,
				'bounds': $scope.polygonOverlays[0].bounds
				//'radius':$scope.sliderValue,
				//'latitude':(marker.position.lat()).toString(),
				//'longitude':(marker.position.lng()).toString()
			};

			NewAlertZone.create(newZone,
				function(data){
					//console.log("Create alertzone Successful");
					//console.log(data);
					$scope.alertZones.push(data);
					data.markerIndex = $scope.alertZones.length - 1;
					$scope.mapIt(data);

					$scope.zoneResponseSuccess = "Alert Zone Created Successfully.";
					$timeout(function(){
			           $scope.zoneResponseSuccess = null;
			        },3000);

					$scope.alertZoneName = null;
					//$scope.sliderValue = 500;
					//circle.set('radius',$scope.sliderValue);
					$scope.geofenceSpinner = false;
					$scope.toggleNewZone();



				},
				function(error){
					//console.log("Create alertzone failed");
					//console.log(error);
					$scope.zoneResponseError = "Something went wrong.";
					$timeout(function(){
			           $scope.zoneResponseError = null;
			        },3000);
				}
			);

		}
		else{

			$scope.zoneResponseError = "You need to draw an Alert Zone.";
			$timeout(function(){
			    $scope.zoneResponseError = null;
			},3000);

		}



	};



	$scope.init = function(){


            $scope.map.drawingManager = new google.maps.drawing.DrawingManager({
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [
                        google.maps.drawing.OverlayType.POLYGON,
                        google.maps.drawing.OverlayType.RECTANGLE
                    ]
                },

                polygonOptions: $scope.map.defaults,
                rectangleOptions: $scope.map.defaults
            });


            google.maps.event.addListener($scope.map.drawingManager, 'overlaycomplete', function (event) {
                var wkt;

                //app.clearText();
                $scope.clearMap();

                // Set the drawing mode to "pan" (the hand) so users can immediately edit
                this.setDrawingMode(null);

                // Polygon drawn
                if (event.type === google.maps.drawing.OverlayType.POLYGON || event.type === google.maps.drawing.OverlayType.POLYLINE) {

                    console.log(event.overlay.getPath());
                    // New vertex is inserted
                    google.maps.event.addListener(event.overlay.getPath(), 'insert_at', function (n) {
                        $scope.updateText();
                    });

                    // Existing vertex is removed (insertion is undone)
                    google.maps.event.addListener(event.overlay.getPath(), 'remove_at', function (n) {
                        $scope.updateText();
                    });

                    // Existing vertex is moved (set elsewhere)
                    google.maps.event.addListener(event.overlay.getPath(), 'set_at', function (n) {
                        $scope.updateText();
                    });
                } else if (event.type === google.maps.drawing.OverlayType.RECTANGLE) { // Rectangle drawn
                    // Listen for the 'bounds_changed' event and update the geometry
                    google.maps.event.addListener(event.overlay, 'bounds_changed', function () {
                        $scope.updateText();
                    });
                }

                wkt = new Wkt.Wkt();
                wkt.fromObject(event.overlay);
                $scope.polygonOverlays.push({'geofence': event.overlay,'bounds':wkt.write()});

                });

            return $scope.map;
    }

    /**
	 * Remove the polgons from Map
	 *
	 */
    $scope.clearMap = function () {


        var i;

	    for (i in $scope.polygonOverlays) {
            if ($scope.polygonOverlays.hasOwnProperty(i)) {
            	console.log($scope.polygonOverlays);
                $scope.polygonOverlays[i].geofence.setMap(null);
            }
        }
        $scope.polygonOverlays.length = 0;

        //console.log($scope.polygonOverlays);
    }

    $scope.updateText = function () {
        var wkt = new Wkt.Wkt();
        wkt.fromObject($scope.polygonOverlays[0]);
        //document.getElementById('wkt').value = wkt.write();
    }

    $scope.mapIt = function(data){

    	  var el, obj, wkt;

            el = data.bounds;
            wkt = new Wkt.Wkt();

            try { // Catch any malformed WKT strings
                wkt.read(el);
            } catch (e1) {
                try {
                    wkt.read(el.replace('\n', '').replace('\r', '').replace('\t', ''));
                } catch (e2) {
                    if (e2.name === 'WKTError') {
                        alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
                        return;
                    }
                }
            }

            obj = wkt.toObject($scope.map.defaults); // Make an object
            obj.setMap($scope.map); // Add it to the map
            //console.log(data.markerIndex);
            $scope.markersList[data.markerIndex] = obj;
            return obj;
    }

    $scope.deviceCurrentLocation();



}]);
