'use strict';

var app = angular.module('appCtrls', ['ui.router']);

app.controller('appCtrl',[ '$scope','storage','$timeout','commonFactory','AppFactory','BlockAppFactory', function ($scope,storage,$timeout,commonFactory,AppFactory,BlockAppFactory) {

    //$scope.totalApps = 123;
    var counter = 0;
    $scope.currentDevice = storage.get('default_device');

    AppFactory.query({id:$scope.currentDevice.id},function(data){
            //converting secs into H:M:S format
            for (var k=0; k < data.length; k++) {
                data[k].duration = commonFactory.secsToTime(data[k].duration);
                if(data[k].block_status == 'blocked'){
                  counter++;
                }
            }
            $scope.appList = data;
            $scope.totalApps = data.length;
            $scope.totalBlocked = counter;
    });

    /**
     * Block/Unblock App - Update
     *
     */
    $scope.updateApp = function(app,index){

        var blockStatus;

        blockStatus = app.block_status == "blocked"?'unblocked':'blocked';
        BlockAppFactory.update({id:app.id,'block_status':blockStatus},function(data){
                console.log("App Updated successfully");
                console.log(data);
                data.duration = commonFactory.secsToTime(data.duration);
                $scope.appList[index] = data;
                $scope.appUpdateSuccess = true;
                $timeout(function(){
                   $scope.appUpdateSuccess = null;
                },3000);
            },function(error){
                console.log(error);
                $scope.appUpdateError = true;
                $timeout(function(){
                   $scope.appUpdateError = null;
                },3000);
        });

    }


 }]);
