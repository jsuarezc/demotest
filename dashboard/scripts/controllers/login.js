'use strict';

var app = angular.module('loginCtrls', ['ui.router']);

app.controller('loginCtrl',['$scope','$rootScope','$location','storage','$timeout','LoginFactory','SignUpFactory',function ($scope,$rootScope,$location,storage,$timeout,LoginFactory,SignUpFactory) {

	if(storage.get('oauthToken')){
		$location.path('/activity');
	}

	$scope.switch = function(container){
		$scope.ifSingup = (container == 'signin') ? false : true;
	};

	$scope.login = function(){
		console.log("I am gonna login baby");
		console.log($scope.userLogin);
		LoginFactory.create($scope.userLogin,
			function(data){
				console.log("Login Successful");

				storage.set('oauthToken',data.token);
				storage.set('default_device',data.default_device);
				if(data.default_device){
					// change the path
					$location.path('/activity');
				}
				else{
					$location.path('/nodevices');
				}	
			},
            function(error){
                console.log("Login failed");
                console.log(error.description[0]);
                $scope.loginFailResponse = error.description[0];
                $timeout(function(){
                    $scope.loginFailResponse = "";
                    $scope.reset(true);
                }, 3000);
            }
        );

	};

	$scope.signUp = function(){
	
		if($scope.newUser.password != $scope.newUser.password_confirm){
			$scope.signUpFailResponse="Passwords didnt match";
			$timeout(function(){
                    $scope.signUpFailResponse = "";
            }, 3000);
		}
		else
		{
			console.log("Gonna create a new user");
			SignUpFactory.create($scope.newUser,
				function(data){
					console.log("Login Successful");
					storage.set('oauthToken',data.token);
					storage.set('default_device',data.default_device);
					if(data.default_device){
						// change the path
						$location.path('/activity');
					}
					else{
						$location.path('/nodevices');
					}	
				
					
		        },
	            function(error){
	                console.log("SignUp failed");
	                console.log(error.data.errors[0]);
	                $scope.signUpFailResponse = error.data.errors[0];
	                $timeout(function(){
	                    $scope.signUpFailResponse = "";
	                    $scope.reset(true);
	                }, 3000);
	            }
	        );
		}
	};

	$scope.reset = function(login) {
		if(login){
			$scope.loginForm.$setPristine();
        	$scope.userLogin = '';
    	}
      
    };

}]);