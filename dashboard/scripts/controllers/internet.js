'use strict';

var app = angular.module('internetCtrls', ['ui.router']);

app.controller('internetCtrl',[ '$scope','$timeout','storage','commonFactory','WebFactory','Whitelist','WhitelistCreate','WhitelistDelete', function ($scope,$timeout,storage,commonFactory,WebFactory,Whitelist,WhitelistCreate,WhitelistDelete) {


	$scope.currentDevice = storage.get('default_device');
	$scope.webHistoryList = [];
	$scope.allowedDomains = [];


    /**
     * Web History - Get
     *
     */
	WebFactory.query({id:$scope.currentDevice.id},
		function(data){
			//console.log(data);
			//console.log("Webhistory Get - Successful");
			//console.log(data.length);
			if(data.length > 0){
				$scope.webHistoryEmpty = false;
        for (var k=0; k<data.length; k++) {
            if(data[k].id){
                data[k].duration = commonFactory.secsToTime(data[k].duration);
                data[k].start_at = commonFactory.unixToDate(data[k].start_at);
                $scope.webHistoryList.push(data[k]);
            }
        }
			}
			else{
				$scope.webHistoryEmpty = true;
			}

		},
		function(error){
			console.log(error);
			console.log("Webhistory Get - Failed");
		}
	);


    /**
     * Whitelist(Allowed Domain) - Get
     *
     */
	Whitelist.query({id:$scope.currentDevice.id},
		function(data){
			//console.log(data);
			//console.log(data.length)
			if(data.length > 0){
				$scope.allowedSitesEmpty = false;
				$scope.allowedDomains = data;
			}
			else{
				$scope.allowedSitesEmpty = true;
			}
		},
		function(error){
			console.log(error);
			console.log("Get White List Failed");
		}
	);


	/*
	$scope.selected = 'webHistory';
	$scope.showWebHistory = function(event){
		$scope.selected = 'webHistory';
		$('#webHistoryList').show();
		$('#timeSpentList').hide();
	};

	$scope.showTimeSpent = function(event) {
		$scope.selected = 'timeSpent';
		$('#timeSpentList').show();
		$('#webHistoryList').hide();
	}; */

	/**
     * Whitelist(Allowed Domains) - Create
     *
     */
	$scope.allowUrl = function(){
		//console.log($scope.urlName);
		//console.log("I am gonna allow you baby.");

		var partsOfStr = $scope.urlName.split(',');
            for(var str in partsOfStr){
                var urlObj = $scope.parseUri(partsOfStr[str]);

                var url = urlObj.protocol+'://'+urlObj.host;
                var domain = $scope.getDomain(urlObj.host);


                if($scope.validateUrl(url)){
                   if ($scope.containsObject(url))
                     {
                        $scope.urlExists = url+" already listed.";
                        $timeout(function(){
                            $scope.urlExists = "";
                        }, 3000);
                     }
                     else{
                         if(url.trim() !== ""){
                             //console.log("Gonna push to server");
                             WhitelistCreate.create(
                             	{
							 	 "device_id": $scope.currentDevice.id,
								  "url" : url,
								  "domain":domain
								},
								function(data){
									//console.log(data);
									$scope.allowedSitesEmpty = false;
									$scope.responseSuccess="Url added Successfully.";
									$scope.allowedDomains.push(data);
									$timeout(function(){
			                            $scope.responseSuccess = "";
			                        }, 3000);

								},
								function(error){
									console.log(error);
									$scope.responseSuccess="Something went wrong.";
									$timeout(function(){
			                            $scope.responseSuccess = "";
			                        }, 3000);
								}
							);

                          }
                     }

                }
                else{
                    $scope.urlExists = "Not a valid URL";
                    $timeout(function(){
                        $scope.urlExists = "";
                    }, 3000);
                }

            }

        $scope.urlName = '';
	};

	/**
     * Whitelist(Allowed Domains) - Delete
     * @param {String} - url, {String} - index
     */

	$scope.deleteSite = function(url,index){
		WhitelistDelete.delete({id:url.id},function(data){
			$scope.allowedDomains.splice(index,1);
			$scope.responseSuccess="Url deleted Successfully.";
			$timeout(function(){
			    $scope.responseSuccess = "";
			}, 3000);
			//console.log(data);
		},function(error){
			console.log(error);
			$scope.responseSuccess="Something went wrong.";
			$timeout(function(){
			    $scope.responseSuccess = "";
			}, 3000);
		});
	};

	/**
     * Check if URL exists in allowedDomains list
     * @param {String} url
     */
	$scope.containsObject = function(url){
		//console.log(typeof $scope.allowedDomains);
		if($scope.allowedDomains){
			//console.log($scope.allowedDomains.length);
			var i;
		    for (i = 0; i < $scope.allowedDomains.length; i++) {
		    	//console.log($scope.allowedDomains[i]['domain']);
		        if ($scope.allowedDomains[i]['domain'] === url) {
		            return true;
		        }
		    }

		    return false;
		}

		return false;


	};

	var options = {
		strictMode: false,
		key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
		q:   {
			name:   "queryKey",
			parser: /(?:^|&)([^&=]*)=?([^&]*)/g
		},
		parser: {
			strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
			loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
		}
	};

    /**
     * URL - Parse
     * @param {String} str
     */
    $scope.parseUri = function(str){
      	var o   = options,
    	   	m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
         	uri = {},
         	i   = 14;

        while (i--) uri[o.key[i]] = m[i] || "";

        uri[o.q.name] = {};
        uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        	if ($1) uri[o.q.name][$1] = $2;
        });
       	return uri;
    };

    /**
     * URL - Validate
     * @param {String} url
     */
    $scope.validateUrl = function(url){
       	return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    };

    /**
     *	Get Domain
     *
     */
     $scope.getDomain = function(str){
     	//return str.replace(([0-9a-z-]{2,}\.[0-9a-z-]{2,3}\.[0-9a-z-]{2,3}|[0-9a-z-]{2,}\.[0-9a-z-]{2,})$, '');
     	var re = /(https?:\/\/)?(w{3}.)?([0-9a-z-]{2,}.[0-9a-z-]{2,3}.[0-9a-z-]{2,3}|[0-9a-z-]{2,}.[0-9a-z-]{2,})\/?$/;

		var m = re.exec(str);

 		return m[0];
     }

}]);
