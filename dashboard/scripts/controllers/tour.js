'use strict';

var app = angular.module('tourCtrls',[]);

app.controller('MobileminderTour',['$scope','$location','storage','$timeout',function ($scope,$location,storage,$timeout) {

  var $demo, duration, remaining, tour;
	  $demo = $("#openDemo");
	  duration = 5000;
	  remaining = duration;
	  tour = new Tour({
	    onStart: function() {
	      return $demo.addClass("disabled", true);
	    },
	    onEnd: function() {
	      return $demo.removeClass("disabled", true);
	    },
	    debug: true,
	    steps: [
	   {
	        //path: "/",
	        element: "#mmBrandLogo",
	        placement: "bottom",
	        title: "Welcome",
	        content: "Welcome to MobileMinder. You will now be taken through a short tutorial to familiarize yourself with the feature set. You can access this tutorial anytime by selecting the ‘Help’ button on the right of the screen.",
	        backdrop: true,
	        //duration: 2000
	      	
	      }, {
	        //path: "/",
	        element: "#headerBtns",
	        placement: "bottom",
	        title: "Control Panel",
	        content: "These buttons control how you administer your account. Clicking your child’s name will drop down a list of your other children if you have also added them to the account.<br><br>The ‘Sign Out’ button will log you out of your account.<br>‘Help’ will restart this tutorial at any time.<br><br>The last symbol will open your account settings where you can change a number of your settings including email alerts for your ‘Alert Zones’ and the emergency numbers your child will contact in case of an emergency.",
	        backdrop: true,
	        //duration: 2000
	      }, {
	        //path: "/api",
	        element: "#profileImgContainer",
	        placement: "bottom",
	        title: "Profile",
	        content: "You can add/edit a profile picture for your child here as well as enter their name.",
	        backdrop: true,
	        //duration: 2000
	        //reflex: true
	      }, {
	       // path: "/api",
	        element: "#activityNav",
	        placement: "right",
	        title: "Activity Overview",
	        content: "Take a one look view of what has been happening your child's device.<br><br>You will be able to view recent contacts, photos taken, texts sent or received, and apps and web usage. Clicking into the specific tabs will provide further detail.",
	        backdrop: true,
	        //duration: 2000
	        //duration: 5000
	      }, {
	        //path: "/api",
	        element: "#locationNav",
	        placement: "right",
	        title: "Location",
	        content: "In the location section you will be shown a map view with the last reported location of your child. Remember they will need to be connected to WiFi or have GPS turned on to report a location point.<br><br>You can also administer your ‘Alert Zones’ on this tab. ‘Alert Zones’ are areas which you select in order to receive updates concerning your children’s movements in and out of these areas. Create and edit ‘Alert Zones’ on the ‘Alert Zones’ tab and then check the status of these zones on the ‘Alerts’ tab.",
	        backdrop: true,
	        //duration: 2000
	      }, {
	        //path: "/",
	        element: "#callsAndTextNav",
	        placement: "right",
	        title: "Call & Texts",
	        content: "The ‘Calls & Texts’ section is made up of your child’s contact list, texts, calls and your flagged words.<br><br>Their texts will be broken down into ‘All’ of their messages and ‘Flagged’ messages that contain flagged words. You will be able to toggle between the two as you please.<br><br>The ‘Flagged Words’ tab is used for you to add your own flagged words you want to be alerted to or delete existing flagged words from our database.Your child’s calls will be available to view showing you how many incoming and outgoing calls they have made and to whom.<br><br>All of this information will be shown depending on what contact in your child’s contact list you want to view.",
	        backdrop: true,
	        //duration: 2000
	      }, {
	        //path: "/api",
	        element: "#galleryNav",
	        placement: "right",
	        title: "Gallery",
	        content: "In the Gallery section you can view all of the photos saved to your child’s phone. When you browse through them you can also remotely delete an image off your child’s phone if you deem it to be unsuitable or offensive.",
	        backdrop: true,
	        //duration: 2000
	        //reflex: true
	      }, {
	       // path: "/api",
	        element: "#appsNav",
	        placement: "right",
	        title: "Apps",
	        content: "Scroll through an entire list of your child’s applications and choose whether or not to allow them access to specific apps. Simply toggle the lock icon to block access to an app.<br><br>NOTE: Be sure to block their internet browser apps so they will only have access to the MobileMinder browser.",
	        backdrop: true,
	        //duration: 2000
	      }, {
	        //path: "/api",
	        element: "#internetNav",
	        placement: "right",
	        title: "Internet",
	        content: "Use the tabs on in the ‘Internet’ section to control your child’s internet usage. View their history, block websites based on URL, keyword and category to ensure that they don’t view harmful material when they are surfing online.",
	        backdrop: true,
	        //duration: 2000
	      }, 
	    ]
	  });
	

    /**
     * Launch Demo 
     * 
     */
	$scope.openDemo = function(){
		console.log("inside the demo");
		tour.restart(true);
	}



    /**
     * Auto start the demo when signup or first login 
     * 
     */	
	if(storage.get('login_count') == 0 || storage.get('login_count') == 1){
		//console.log("login count zero");
		//console.log(storage.get('login_count'));
		// Start the tour
		$timeout(function () { tour.restart(true); }, 1000);
		
	}	

}]);