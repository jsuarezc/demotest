'use strict';

var app = angular.module('paymentCtrls', ['ui.router']);

app.controller('paymentCtrl',['$scope','storage','StripeCredentils','StripeCurrentUser','StripePayment', 'StripeCoupon', function ($scope,storage,StripeCredentils,StripeCurrentUser,StripePayment, StripeCoupon) {


    $scope.stripeKey,$scope.currentUserInfo = [];

    $scope.init = function(){
        console.log("inside the current key");
        $scope.currentDevice = storage.get('default_device');
    }

    /**
     * Stripe Credentials - Get
     *
     */
    StripeCredentils.query(
        function(data){
            //console.log('Stripe Credentials Get Success');
            //console.log(data);
            console.log('Stripe',data);
            if(data){
                $scope.stripeKey = data.publish_key
            }
            else{
                console.log("Stripe return empty data object");
                console.log(data);
            }

        },
        function(error){
            console.log("Stripe Credentials Get Failure");
            console.log(error);
        }
    );

    //$scope.stripeKey = 'pk_test_1pIBTYuXKiAC9TEAFAlGPscy';

    /**
     * Stripe Current User - Info
     *
     */
    StripeCurrentUser.query(
        function(data){
            console.log("Stripes Current User Info Success");
            console.log(data);
            if(data){
                $scope.currentUserInfo = data;
            }
        },
        function(error){
            console.log("Stripes Current User Info Failure");
            console.log(error);
        }
    );

    /**
     * GET coupon
     *
     */
    $scope.getCoupon = function (obj) {
      var $couponButton = $(obj.currentTarget);
      var $couponInput = $couponButton.prev();
      var coupon_code = $couponInput.val();
      var $price = $couponInput.closest('.planTextDiv').find('.price');

      $couponButton.attr("class","btn").attr("disabled", true).text("Checking...");
      $couponInput.attr('disabled', 'true');

      StripeCoupon.query({"coupon_code":coupon_code},
        function(coupon){
          $couponButton.attr("class","btn-success btn").text("OK");

          $scope.coupon_code = coupon.id;
          var existingPrice = parseFloat($price.text()) * 100;
          if (!(coupon.percent_off === null)) {
            $scope.price = parseInt(existingPrice * ((100 - coupon.percent_off) / 100));
          } else {
            $scope.price = parseInt(existingPrice - coupon.amount_off);
          }
          $price.text($scope.price / 100);
        },
        function(error) {
          $couponButton.attr("class","btn").text("Not valid").removeAttr("disabled");
          $couponInput.removeAttr('disabled');
        }
      );
    }

    /**
     * Submit Payment
     *
     */
    $scope.submitPayment = function(plan){
        $scope.plan = plan;
        var handler = StripeCheckout.configure({
            key: $scope.stripeKey,
            email:$scope.currentUserInfo.email,
            token: function(token) {
                var params = {
                               "device_id":$scope.currentDevice.id,
                               "stripe_email":$scope.currentUserInfo.email,
                               "stripe_plan":$scope.plan,
                               "stripe_token": token.id,
                               "stripe_new_card": 'false',
                               "coupon" : $scope.coupon_code
                             };
                StripePayment.create(params,
                    function(data){
                        //console.log(data);
                    },
                    function(error){
                        //console.log(error);
                    }
                );

            }
          });


           switch(plan){
            case 'single_3999usd_year':
                var price = 3999;
                if (!(typeof $scope.price === 'undefined')) {
                  price = $scope.price;
                };
                handler.open({
                  name: 'One Device',
                  description: 'One Year Service',
                  amount: price,
                  currency:"USD",

                });
                break;
            case 'multiple_9999usd_year':
                var price = 9999;
                if (!(typeof $scope.price === 'undefined')) {
                  price = $scope.price;
                };
                handler.open({
                  name: 'Multiple Devices',
                  description: 'One Year Service',
                  amount: price,
                  currency:"USD",
                });
                break;
           }

    }

}]);
