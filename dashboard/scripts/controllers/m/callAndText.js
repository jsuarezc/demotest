'use strict';

var app = angular.module('callAndTextCtrls', ['ui.router']);

app.controller('callAndTextCtrl',['$scope','$timeout','storage','commonFactory','ContactsFactory','ContactsInfo','TextsFactory','CallsFactory','DefaultFlaggedWordsFactory','FlaggedWordsFactory','FlaggedWordsFactoryCreate','FlaggedWordsFactoryDelete','DefaultFlaggedWordsFactoryDelete', function ($scope,$timeout,storage,commonFactory,ContactsFactory,ContactsInfo,TextsFactory,CallsFactory,
  DefaultFlaggedWordsFactory,FlaggedWordsFactory,FlaggedWordsFactoryCreate,FlaggedWordsFactoryDelete,DefaultFlaggedWordsFactoryDelete) {



    $scope.msgHeader = false;
    $scope.showFlaggedDiv = false;
    $scope.showAllMesgDiv = false;
    $scope.showCallsDiv = false;
    $scope.defaultFlaggedWordsDiv = false;
    $scope.flaggedWordsDiv = true;
    $scope.contactId;
    $scope.lastMsgTimestamp,$scope.lastCallTimestamp;

    $scope.currentDevice = storage.get('default_device');

    $scope.defaultProfilePic = 'https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/images/contacts/default';
    console.log($scope.currentDevice);

    $scope.addTransparentDiv = function(){
        console.log("Add the transparent Div");
        var destination = $('#contactsTableDiv').offset();
        $scope.highlightContactId = $scope.selectedContactId;
        $scope.selectedContactId = null;


        $('.overlayContainer').css({width: $('#contactsTableDiv').width(), height: $('#contactsTableDiv').height()});
        $('.overlayContainer').css({top: destination.top, left: destination.left});
        $('.overlayContainer').show();
    }
    $scope.removeTransparentDiv = function(){
        console.log("Remove the transparent Div");
        if ( $('.overlayContainer').is(':visible') ){
            $('.overlayContainer').hide();
            $scope.selectedContactId = $scope.highlightContactId;
        }
    }

    $scope.showContacts = function(){
        $('#contactsInfoDiv').hide();
        $('#contactsTableDiv').show();
    };

    /**
     * Contact List - Get
     * @return {Array}
     */
    ContactsFactory.query({id:$scope.currentDevice.id},
        function(data){
            console.log("Get contacts success.");
            console.log(data);
            for (var k=0;k<data.length;k++){
                if(data[k].id){
                  data[k].index = k;
                 }
            }

            $scope.contacts = data;
        },
        function(error){
            console.log("Get contacts failed.");
            console.log(error);
        }
    );



    /**
     * Flagged Words - Add
     *
     */
    $scope.addFlaggedWord = function(){
        console.log("Add a flagged words");
        console.log($scope.newFlaggedWord);
        var param = {
                        "device_id" : $scope.currentDevice.id,
                        "word": $scope.newFlaggedWord.toLowerCase()
                     };
        $scope.spinner = true;

        FlaggedWordsFactoryCreate.create(param,
           function(data){
             console.log(data);

             $scope.flaggedWords.push(data);
             $scope.spinner = false;
             $scope.newFlaggedWord = '';
           },
           function(error){
             console.log(error);
             if(error.status == 400){
                $scope.spinner = false;
                $scope.flaggedWordError = error.data.description[0];
                $timeout(function() {
                    $scope.flaggedWordError = '';
                }, 3000);
             }
             else{
                $scope.spinner = false;
                $scope.flaggedWordError = error.data.description[0];
                $timeout(function() {
                    $scope.flaggedWordError = '';
                },3000);
             }
             $scope.newFlaggedWord = '';

           }
        );
    };


    /**
     * Default Flagged Words - Delete
     *
     */
    $scope.deleateDefaultFlaggedWord  = function(flagged,index){
        console.log(flagged);
        $scope.spinner = true;
        DefaultFlaggedWordsFactoryDelete.delete({id:flagged.id},
          function(data){
             console.log(data);
             $scope.spinner = false;
             $scope.defaultFlaggedWords.splice(index, 1);

          },
          function(error){
             $scope.spinner = false;
             console.log(error);
          }
        );
    };

    /**
     * Flagged Words - Delete
     *
     */
    $scope.deleateFlaggedWord  = function(flagged,index){
        console.log(flagged);
        $scope.spinner = true;
        FlaggedWordsFactoryDelete.delete({id:flagged.id},
          function(data){
             console.log(data);
             $scope.spinner = false;
             $scope.flaggedWords.splice(index, 1);

          },
          function(error){
             $scope.spinner = false;
             console.log(error);
          }
        );
    };
    /**
     * @brief: Flagged Words - Get
     *
     */
     $scope.flaggedWords = FlaggedWordsFactory.query({'id':$scope.currentDevice.id},
          function(data){
            console.log("Get Flagged Words Successful");
            console.log(data);
          },
          function(error){
            console.log("Get Flagged Words Failed");
            console.log(error);
          }
     );
    /**
     * Default Flagged Words - Get
     *
     */
    $scope.defaultFlaggedWords = DefaultFlaggedWordsFactory.query({'id':$scope.currentDevice.id},
        function(data){
          console.log(data);
          console.log("Get Default FlaggedWords Success");
        },
        function(error){
          console.log(error);
          console.log("FlaggedWords Error");
        }
    );

    $scope.contactInfo = function(contact,index){
      //console.log(contact);
      $('#contactsTableDiv').hide();
      $('#contactsInfoDiv').show();

      $scope.currentContact = contact;

      ContactsInfo.query({id:contact.id},
        function(data){
            //console.log("Contact Info Success");
            $scope.flaggedMsgsReceived = data.total_received_flagged_texts;
            $scope.flaggedMsgsSent = data.total_sent_flagged_texts;
            $scope.msgsSend = data.total_sent_texts;
            $scope.msgsReceived = data.total_received_texts;
            $scope.totalCalls = data.total_calls;
            $scope.incomingCallDuration = data.total_incoming_duration;
            $scope.outGoingCallDuration = data.total_outgoing_duration;

            $scope.selectedContactId  = index;
            $scope.currentContactId = contact.id;
            $scope.loadMsgs(contact.id);
            $scope.loadFlaggedMsgs(contact.id);
            $scope.loadCalls(contact.id);
            //console.log(data);
        },
        function(error){
          console.log("Contact Info Failed");
          console.log(error);
        }
      )
    };


    /**
     * Flagged Messages - Get
     *
     */
    $scope.loadFlaggedMsgs = function(id,lazyloading){
      $scope.msgHeader = true;
      var param = {};

      if(lazyloading){
         param = {'id':id,'before':$scope.lastFlaggedMsgTimestamp,'is_flagged':'true'};
      }
      else{
         $scope.flaggedMsgs = [];
         $scope.showFlaggedDiv = true;
         $scope.showAllMesgDiv = false;
         param = {'id':id,'order_by':'timestamp','order_type':'desc','is_flagged':'true'};
         //$scope.msgsReceived = 0,$scope.msgsSend = 0,$scope.flaggedMsgsReceived = 0, $scope.flaggedMsgsSent = 0;
      }

      TextsFactory.query(param,
        function(data){
            for (var k=0;k<data.length;k++) {
                if(data[k].timestamp){
                  $scope.lastFlaggedMsgTimestamp = data[k].timestamp;
                  //(data[k].direction == 'incoming') ? $scope.msgsReceived++:$scope.msgsSend++;
                  //console.log("flagged messages");
                  //console.log(data[k].is_flagged);
                  if(data[k].is_flagged){
                     console.log("inside the flagged word true");
                     data[k].message = data[k].message.toLowerCase();
                     for (var i = 0; i < data[k].flagged_words.length; i++) {
                          console.log(data[k].message.indexOf(data[k].flagged_words[i]) > -1);
                          if(data[k].message.indexOf(data[k].flagged_words[i]) > -1){
                            data[k].message = data[k].message.replace(data[k].flagged_words[i],'<code>'+data[k].flagged_words[i]+'</code>')
                          }
                     }

                  }
                }

                data[k].timestamp = commonFactory.unixToDate(data[k].timestamp);

            }

            if($scope.flaggedMsgs.length > 0){
              $scope.flaggedMsgs =  $scope.flaggedMsgs.concat(data);
            }
            else{
              $scope.flaggedMsgs = data;
            }
            console.log($scope.flaggedMsgs.length);
        },
        function(error){
            console.log(error);
        }
      );
    }
 $scope.loadMsgs = function(id,lazyloading){
      $scope.msgHeader = true;
      var param = {};

      if(lazyloading){
         param = {'id':id,'before':$scope.lastMsgTimestamp,'page':1,'per_page':10};
      }
      else{
         $scope.allMsgs = [];
         $scope.showFlaggedDiv = true;
         $scope.showAllMesgDiv = false;
         param = {'id':id,'page':1,'per_page':10 ,'order_by':'timestamp','order_type':'desc'};
         //$scope.msgsReceived = 0,$scope.msgsSend = 0,$scope.flaggedMsgsReceived = 0, $scope.flaggedMsgsSent = 0;
      }

      TextsFactory.query(param,
        function(data){
            for (var k=0;k<data.length;k++) {

                if(data[k].timestamp){
                  $scope.lastMsgTimestamp = data[k].timestamp;
                   if(data[k].is_flagged){
                     console.log("inside the flagged word true");
                     data[k].message = data[k].message.toLowerCase();
                     for (var i = 0; i < data[k].flagged_words.length; i++) {
                          if(data[k].message.indexOf(data[k].flagged_words[i]) > -1){
                            data[k].message = data[k].message.replace(data[k].flagged_words[i],'<code>'+data[k].flagged_words[i]+'</code>')
                          }
                     }

                  }
                  //(data[k].direction == 'incoming') ? $scope.msgsReceived++:$scope.msgsSend++;
                }

                data[k].timestamp = commonFactory.unixToDate(data[k].timestamp);


            }

            if($scope.allMsgs.length > 0){
              $scope.allMsgs =  $scope.allMsgs.concat(data);
            }
            else{
              $scope.allMsgs = data;
            }
            console.log($scope.allMsgs.length);
        },
        function(error){
            console.log(error);
        }
      );
    }

    $scope.loadCalls = function(id,lazyloading){
      $scope.showCallsDiv = true;

      var param = {};

      if(lazyloading){
         param = {'id':id,'before':$scope.lastCallTimestamp,'page':1,'per_page':10,'order_by':'timestamp','order_type':'desc'};
      }
      else{
         $scope.callList = [];
         param = {'id':id,'page':1,'per_page':10,'order_by':'timestamp','order_type':'desc'};
         $scope.incomingCallsDuration = 0,$scope.outgoingCallsDuration = 0;
      }


      CallsFactory.query(param,
        function(data){
          for (var k=0;k<data.length; k++) {
            if(data[k].timestamp){
                $scope.lastCallTimestamp = data[k].timestamp;
                //(data[k].direction == 'incoming') ? $scope.flaggedMsgsReceived++:$scope.flaggedMsgsSent++;

            }
            data[k].timestamp = commonFactory.unixToDate(data[k].timestamp);
            data[k].duration = commonFactory.secsToTime(data[k].duration);
          }
          console.log($scope.callList.length);
          if($scope.callList.length > 0){
            $scope.callList =  $scope.callList.concat(data);
          }
          else{
            $scope.callList = data;
          }
          console.log($scope.callList.length);
        },
        function(error){
          console.log(error);
          console.log("Get calls Error");
        }
      );
    }

    $scope.showFlaggedMsgs = function(){
      console.log("Show flagged messages");
      $scope.showAllMesgDiv = false;
      $scope.showFlaggedDiv = true;
    }

    $scope.showAllMsgs = function(){
      console.log("Show all messages");
      $scope.showFlaggedDiv = false;
      $scope.showAllMesgDiv = true;
    }

    $scope.showFlaggedWords = function(){
      console.log("Show flagged words");
      $scope.flaggedWordsDiv = true;
      $scope.defaultFlaggedWordsDiv = false;
    };
    $scope.showDefaultFlaggedWords = function(){
      console.log("Show default flagged words");
      $scope.defaultFlaggedWordsDiv = true;
      $scope.flaggedWordsDiv = false;
    };



    $('#allMessagesContainerDiv').bind('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
            $scope.loadMsgs($scope.currentContactId,true);
        }
    });

    $('#allCallsContainer').bind('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
            //$scope.loadMsgs($scope.selectedContactId,true);
            console.log("gonna load more calls");
            $scope.loadCalls($scope.currentContactId,true);
        }
    })



  }]);
