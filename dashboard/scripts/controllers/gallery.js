'use strict';

var app = angular.module('galleryCtrls', ['ui.router']);

app.controller('galleryCtrl',[ '$scope','storage','GalleryFactory','DeviceCommandFactory','Amazon',function ($scope,storage,GalleryFactory,DeviceCommandFactory,Amazon) {

    $scope.imageListUrls = [];

    $scope.currentDevice = storage.get('default_device');


    AWS.config.update({  
        accessKeyId: storage.get('access_key'),
        secretAccessKey: storage.get('secret_key'),
        sessionToken:storage.get('session_token'),
        region: 'eu-west-1',
        logger: 'console'

    });



    // create the AWS.Request object
    var bucket = new AWS.S3();
    var param = {Bucket: 'media-mobileminder-com',Prefix:'images/devices/'+$scope.currentDevice.id+'/',Delimiter:'/'};
    bucket.listObjects(param,function (err, data) {
        if(err) {
            console.log("Get Objects Failed");
            console.log(err);
        }else {
            //console.log("Get Objects Successful");
            //console.log(data.Contents);
              if(data.Contents.length > 0){
                    angular.forEach(data.Contents, function(value, key) {
                         //console.log(value.Key);
                       
                             //var params = {Bucket: 'media.mobileminder.com', Key: value.Key};
                             //var url = bucket.getSignedUrl('getObject', params);
                             var url = "https://s3-eu-west-1.amazonaws.com/media-mobileminder-com/"+value.Key
                             $scope.imageListUrls.push({ id:key,url:url,key:value.Key});
                   
                     });
                    $scope.$apply(function () {
                        $scope.imageList = $scope.imageListUrls;
                        //console.log($scope.imageList);
                    });
              }
              else{
                 
                  $scope.$apply(function () {
                       $scope.noImages = true;
                    });
              }
           
        }
    });





    $scope.enlargeImage = function(image,index){
        //console.log(image);
        //console.log(index);
        //console.log($scope.imageList.length);
        $('#galleryModal').modal('show');

        $scope.currentImage = image;
        $scope.currentIndex = index;

        if($scope.imageList.length == 1){
            $scope.hidePrevBtn = true;
            $scope.hideNextBtn = true;
        }
        else if($scope.imageList.length > 0){

            $scope.hidePrevBtn = ($scope.currentIndex == 0)? true: false;
            $scope.hideNextBtn = ($scope.currentIndex == $scope.imageList.length - 1)?true:false;
        }

    };

    $scope.prev = function(currentIndex,image){

        //console.log(image);

        if($scope.imageList.length == 1){
            $scope.currentIndex = currentIndex;
            $scope.currentImage = $scope.imageList[$scope.currentIndex];   
            $scope.hidePrevBtn = true;
            $scope.hideNextBtn = true;
        }
        else if($scope.imageList.length > 0){
            $scope.currentIndex = currentIndex - 1;
            $scope.currentImage = $scope.imageList[$scope.currentIndex];

            $scope.hidePrevBtn = ($scope.currentIndex == 0)? true: false;
            $scope.hideNextBtn = ($scope.currentIndex == $scope.imageList.length - 1)?true:false;

        }

    };

    $scope.next = function(currentIndex,image){	
        //console.log($scope.currentIndex);
        //console.log("Inside the next "+$scope.currentIndex);
        //console.log(image);

        if($scope.imageList.length == 1){
            $scope.currentIndex = currentIndex;
            $scope.currentImage = $scope.imageList[$scope.currentIndex];        
            $scope.hidePrevBtn = true;
            $scope.hideNextBtn = true;
        }
        else if($scope.imageList.length > 0){
            $scope.currentIndex = currentIndex + 1;
            $scope.currentImage = $scope.imageList[$scope.currentIndex];            
            $scope.hidePrevBtn = ($scope.currentIndex == 0)? true: false;
            $scope.hideNextBtn = ($scope.currentIndex == $scope.imageList.length - 1)?true:false;
        }
    	//var nextImage = $scope.imageList[currentImage.id];
    	
    };

    $scope.deleteImage = function(currentIndex,image){

    	console.log("gonna delete you baby");
        console.log($scope.imageList);
        console.log(currentIndex);

        //$scope.disableDeleteBtn = true;

        $scope.spinner = true;
  

        $scope.paramObj = {Bucket: 'media-mobileminder-com',Key:$scope.imageList[currentIndex].key}

        bucket.getObject($scope.paramObj,function (err, data) {
               if(err) {
                    console.log("Get Objects Failed");
                    console.log(err);
                }
                else {
                    console.log(data);
                    console.log("Get Object Successful");
                    console.log(data.Metadata);
                    if(data.Metadata.path){
                        //$scope.$broadcast('deleteObjectCommand', data.Metadata.path);
                        $scope.deleteImageCommand(currentIndex,image,data.Metadata.path);
                    }
                    else{
                        $scope.spinner = false;
                        $scop.imageDeletionError = "Image doesnt have a metapath!";
                        $scope.$apply(function () {
                            $scop.imageDeletionError = '';
                        });
                    }

                }
        });       

        
    };

    $scope.deleteImageCommand = function(currentIndex,image,metadataPath){
        console.log("Delete Object Successful");
        var command = { 
                        "id":$scope.currentDevice.id,
                        "kind":"filesystem",
                        "properties": {
                            "action":"remove",
                            "arguments": {
                                            "path":metadataPath
                                         }                   
                        }
                      };
            DeviceCommandFactory.create(command,
                function(data){
                    console.log(data);
                    console.log("Image deletion command Successful");
                    $scope.deleteImageOnAmazon(currentIndex,image); 
                },
                function(error){
                    console.log(error);
                    console.log("Image deletion command Failed");
                    $scope.spinner = false;
                    $scop.imageDeletionError = "Image deletion failed.";
                    $scope.$apply(function () {
                        $scop.imageDeletionError = '';
                    });
                }
            );   
    };

    $scope.deleteImageOnAmazon = function(currentIndex,image){

        bucket.deleteObject($scope.paramObj,function (err, data) {
                if(err) {
                    console.log("Object deletion Failed");
                    console.log(err);
                    $scope.spinner = false;
                    $scop.imageDeletionError = "Image deletion failed.";
                    $scope.$apply(function () {
                        $scop.imageDeletionError = '';
                    });
                }
                else { 
                    console.log(data);
                    console.log("Object deletion Successful");                                   
                    //$scope.$broadcast('deletionSuccess', 'success'); 

                    $scope.spinner = false;
                    if($scope.imageList.length == 1){
                        console.log("Deleting the image");
                        console.log($scope.imageList);
                        console.log($scope.imageList.length);
                        console.log($scope.currentIndex);
                        $scope.$apply(function () {
                            $scope.imageList.splice($scope.currentIndex,1);
                            $scope.currentIndex = currentIndex - 1; 
                            //$scope.imageList = $scope.imageList.filter(function(e) { return e.key !==  image.key; }); 

                            //$scope.imageList = $scope.imageList.filter(function(e) { return e.Key !== "Kristian"; });
                        });
                        $('#galleryModal').modal('hide');
                        $scope.noImages = true;
                    }
                    else if($scope.imageList.length > 0) {
                            if($scope.currentIndex == 0){
                                console.log("currentIndex is 0");
                                $scope.$apply(function () {
                                    $scope.imageList.splice(currentIndex,1);
                                    //$scope.imageList = $scope.imageList.filter(function(e) { return e.key !==  image.key; });  
                                    $scope.currentIndex = currentIndex + 1; 
                                    $scope.currentImage = $scope.imageList[$scope.currentIndex];
                                });
                                
                            }
                            else{
                                console.log("Inside the index greater than zero");                             

                                $scope.$apply(function () {
                                    $scope.imageList.splice(currentIndex,1); 
                                    //$scope.imageList = $scope.imageList.filter(function(e) { return e.key !==  image.key; });  
                                    $scope.currentIndex = currentIndex - 1;
                                    $scope.currentImage = $scope.imageList[$scope.currentIndex];
                                });
                                
                            }
                            $scope.hidePrevBtn = ($scope.currentIndex == 0)? true: false;
                            $scope.hideNextBtn = ($scope.currentIndex == $scope.imageList.length - 1)?true:false;
                            //$scope.disableDeleteBtn = false;

                    }            
                    

                    
                }
            });
    }



}]);
